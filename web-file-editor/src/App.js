import React from 'react';
import axios from "axios";
import { Button, Typography } from 'antd';
import './App.css';
import './index.css';


 // J'initialise la constante qui va recevoir le contenu du fichier à modifier avec un string vide
const fileContent = "";

 console.log(process.argv);
 const { Text } = Typography;

class App extends React.Component{
  constructor(props){
    super(props);
    // State qui vient de l'API 
    // this.state={apiResponse: "" };

    // L'état actuel est celui du fileContent déclaré plus haut 
    this.state = {
      str: fileContent,
    };
  }

  callAPI(){
    // J'appelle l'API et la page qui contient ma gestion du fichier 
    fetch("http://localhost:9000/file")
    .then(res => res.text())
    //.then(res => res.replace("[\"",""))
    //.then(res => res.replace("\"]",""))

    //Modification de l'état
    .then(res => this.setState({str:res}));

  }

  componentWillMount(){
    this.callAPI();
  }

    onChange = str => {
      console.log('Content change:', str);
      this.setState({ str });
    };


    handleClick(str) {
      console.log("Button save pressed");
      console.log(str);
      // Envoie une requête à l'API pour lui signaler que le fichier a été modifié et qu'il est prêt à être enregistré localement
      axios.post('http://localhost:9000/file', {
        newContent: str,
      })
      .then(function (response) {
        console.log(response);
      });
    }
    

  render(){
    return (
      <div className="App">
        <header className="center">
          <h1>Modification of the file</h1>
        </header>
        <body>
        <div className="blocContent center">
          <div>
            <Text editable={{ onChange: this.onChange }}>{this.state.str}</Text>
          </div>
          <Button onClick={this.handleClick(this.state.str)} type="primary">Save</Button>
        </div>
      </body>
      <footer>
      </footer>
      </div>
    );
  }
}

export default App;
