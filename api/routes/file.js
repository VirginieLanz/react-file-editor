var express = require('express');
var router = express.Router();
const filename = process.argv[2];
const fs = require('fs');

/* GET file page*/
router.get("/", function(req, res, next){
    // Read file and display content
    fs.readFile(filename, 'utf8', function(err, data) {
        if (err) throw err;
        console.log('OK: ' + filename);
        console.log(data)

        //res.send({filename: filename, content: data});
        res.send(data);
    });
});

router.post('/', function (req, res) {
    // File modified
    console.log("req recieved :::::", req.body.newContent);
    fs.writeFile(filename, req.body.newContent, function (err) { 
            if (err){
                console.log(err);
            }else{
                console.log('Write operation complete.');
                res.send("OK");
            }
    });
});

module.exports = router;
